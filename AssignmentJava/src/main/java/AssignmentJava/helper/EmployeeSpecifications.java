package AssignmentJava.helper;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import AssignmentJava.model.EmployeeModel;

public class EmployeeSpecifications {

	public static Specification<EmployeeModel> withFirstName(String name) {
		return (root, query, builder) -> builder.like(root.get("firstName"), "%" + name + "%");
	}

	public static Specification<EmployeeModel> withEmployeeCompany(Integer companyId) {
		return (root, query, builder) -> builder.equal(root.get("company").get("id"), companyId);
	}

	public static Specification<EmployeeModel> withEmployeeIDNumber(String code) {
		return (root, query, builder) -> builder.like(root.get("code"), "%" + code + "%");
	}

	public static Specification<EmployeeModel> withEmployeeCreatedBy(String createdBy) {
		return (root, query, builder) -> builder.like(root.get("createdBy"), "%" + createdBy + "%");
	}

	public static Specification<EmployeeModel> withEmployeeCreatedDate(LocalDate createdDate) {
		return (root, query, builder) -> builder.equal(root.get("createdDate"), createdDate);
	}

	public static Specification<EmployeeModel> withEmployeeIsDelete(Boolean isDelete) {
		return (root, query, builder) -> builder.equal(root.get("isDelete"), isDelete);
	}
	
}
