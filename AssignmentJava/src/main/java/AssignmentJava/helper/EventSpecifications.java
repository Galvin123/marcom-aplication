package AssignmentJava.helper;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import AssignmentJava.model.EventModel;

public class EventSpecifications {
	
	public static Specification<EventModel> withTransactionCode(String code) {
		return (root, query, builder) -> builder.like(root.get("code"), "%" + code + "%");
	}
	
	public static Specification<EventModel> withRequestEvent(Integer requestBy) {
		return (root, query, builder) -> builder.equal(root.get("request").get("id"), requestBy);
	}
	
	public static Specification<EventModel> withRequestDate(LocalDate requestDate) {
		return (root, query, builder) -> builder.equal(root.get("requestDate"), requestDate);
	}
	
	public static Specification<EventModel> withStatus(Integer status) {
		return (root, query, builder) -> builder.equal(root.get("event"), status);
	}

	public static Specification<EventModel> withEventCreatedBy(String createdBy) {
		return (root, query, builder) -> builder.like(root.get("createdBy"), "%" + createdBy + "%");
	}

	public static Specification<EventModel> withEventCreatedDate(LocalDate createdDate) {
		return (root, query, builder) -> builder.equal(root.get("createdDate"), createdDate);
	}

	public static Specification<EventModel> withEventIsDelete(Boolean isDelete) {
		return (root, query, builder) -> builder.equal(root.get("isDelete"), isDelete);
	}
	
}
