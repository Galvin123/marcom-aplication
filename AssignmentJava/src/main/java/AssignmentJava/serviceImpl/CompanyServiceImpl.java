package AssignmentJava.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import AssignmentJava.Dto.CompanySearchFormDTO;
import AssignmentJava.helper.CompanySpecifications;
import AssignmentJava.model.CompanyModel;
import AssignmentJava.repositories.CompanyRepository;

import AssignmentJava.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository companyRepo;

	@Override
	public List<String> getAllCompanyName() {
		return companyRepo.getAllCompanyName();
	}

	@Override
	public List<String> getAllCompanyCode() {
		return companyRepo.getAllCompanyCode();
	}

	@Override
	public List<Integer> getPageNumbers(Integer totalPages) {
		List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).boxed().collect(Collectors.toList());
		return pageNumbers;
	}

	@Override
	public Page<CompanyModel> getCompanyWithPage(Pageable pageable, CompanySearchFormDTO searchForm) {
		Integer pageSize = pageable.getPageSize();
		Integer currentPage = pageable.getPageNumber();
		Integer startItem = currentPage * pageSize;
		List<CompanyModel> companyList = new ArrayList<CompanyModel>();
		if(searchForm.getCompanyCode()!= null || searchForm.getCompanyName()!=null || searchForm.getCreatedBy()!=null) {
			if(searchForm.getCreatedDate()!=null) {
				companyList = this.companyRepo.findAll(Specification
				.where(CompanySpecifications.withCompanyCode(searchForm.getCompanyCode()))
				.and(CompanySpecifications.withCompanyName(searchForm.getCompanyName()))
				.and(CompanySpecifications.withCreatedDate(searchForm.getCreatedDate()))
				.and(CompanySpecifications.withCreatedBy(searchForm.getCreatedBy()))
				.and(CompanySpecifications.withIsDelete(false)));
			}
			else {
				companyList = this.companyRepo.findAll(Specification
						.where(CompanySpecifications.withCompanyCode(searchForm.getCompanyCode()))
						.and(CompanySpecifications.withCompanyName(searchForm.getCompanyName()))
						.and(CompanySpecifications.withCreatedBy(searchForm.getCreatedBy()))
						.and(CompanySpecifications.withIsDelete(false)));
			}
		}
		else {
			companyList = this.companyRepo.getAllCompany();
		}
        List<CompanyModel> list;
 
        if (companyList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            Integer toIndex = Math.min(startItem + pageSize, companyList.size());
            list = companyList.subList(startItem, toIndex);
        }
 
        Page<CompanyModel> companyPage = new PageImpl<CompanyModel>(list, PageRequest.of(currentPage, pageSize), companyList.size());
        return companyPage;
	}

	@Override
	public String save(CompanyModel company) {
		String message = new String();
		CompanyModel companyForDatabase = new CompanyModel();
		if(company.getId()!=null) {
			companyForDatabase = this.getCompanyFindById(company.getId());
			companyForDatabase.setAddress(company.getAddress());
			companyForDatabase.setEmail(company.getEmail());
			companyForDatabase.setName(company.getName());
			companyForDatabase.setPhone(company.getPhone());
			companyForDatabase.setUpdatedBy("Admin");
			companyForDatabase.setUpdatedDate(LocalDate.now());
			this.companyRepo.save(companyForDatabase);
			message = "Data Updated! Data Company has been updated";
		}
		else {
			Integer maxId = this.companyRepo.getValueId();
			companyForDatabase.setAddress(company.getAddress());
			companyForDatabase.setEmail(company.getEmail());
			companyForDatabase.setName(company.getName());
			companyForDatabase.setPhone(company.getPhone());
			companyForDatabase.setCreatedBy("Admin");
			companyForDatabase.setCreatedDate(LocalDate.now());
			companyForDatabase.setIsDelete(false);
			if(maxId!=null) {
				maxId++;
				companyForDatabase.setCode("CP00"+ maxId.toString());
			}
			else {
				companyForDatabase.setCode("CP001");
			}
			this.companyRepo.save(companyForDatabase);
			message = "Data Saved! New Company has been add with code " + companyForDatabase.getCode() + " !";
		}
		return message;
	}

	@Override
	public CompanyModel getCompanyFindById(Integer id) {
		return this.companyRepo.findById(id).orElse(null);
	}

	@Override
	public String delete(CompanyModel company) {
		CompanyModel companyForDelete = new CompanyModel();
		companyForDelete = this.companyRepo.findById(company.getId()).orElse(null);
		companyForDelete.setUpdatedBy("Admin");
		companyForDelete.setUpdatedDate(LocalDate.now());
		companyForDelete.setIsDelete(true);
		this.companyRepo.save(companyForDelete);
		return "Data Deleted! Data Company with code " + companyForDelete.getCode() + "has been deleted !";
	}
}
