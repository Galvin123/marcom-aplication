package AssignmentJava.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import AssignmentJava.Dto.CompanyDTO;
import AssignmentJava.Dto.EmployeeSearchFormDTO;
import AssignmentJava.helper.EmployeeSpecifications;
import AssignmentJava.model.CompanyModel;
import AssignmentJava.model.EmployeeModel;
import AssignmentJava.repositories.CompanyRepository;
import AssignmentJava.repositories.EmployeeRepository;
import AssignmentJava.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository employeeRepo;

	@Autowired
	private CompanyRepository companyRepo;

	@Override
	public List<Integer> getPageNumbers(Integer totalPages) {
		List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
		return pageNumbers;
	}

	@Override
	public Page<EmployeeModel> getEmployeeWithPage(Pageable pageable, EmployeeSearchFormDTO searchForm) {
		Integer pageSize = pageable.getPageSize();
		Integer currentPage = pageable.getPageNumber();
		Integer startItem = currentPage * pageSize;
		List<EmployeeModel> employeeList = new ArrayList<EmployeeModel>();
		if (searchForm.getEmployeeIdNumber() != null || searchForm.getEmployeeName() != null
				|| searchForm.getCreatedBy() != null) {
			if (searchForm.getCreatedDate() != null && searchForm.getCompany() != null) {
				employeeList = this.employeeRepo.findAll(Specification
						.where(EmployeeSpecifications.withEmployeeIDNumber(searchForm.getEmployeeIdNumber()))
						.and(EmployeeSpecifications.withFirstName(searchForm.getEmployeeName()))
						.and(EmployeeSpecifications.withEmployeeCreatedBy(searchForm.getCreatedBy()))
						.and(EmployeeSpecifications.withEmployeeCompany(searchForm.getCompany()))
						.and(EmployeeSpecifications.withEmployeeCreatedDate(searchForm.getCreatedDate()))
						.and(EmployeeSpecifications.withEmployeeIsDelete(false)));
			} else if (searchForm.getCreatedDate() != null && searchForm.getCompany() == null) {
				employeeList = this.employeeRepo.findAll(Specification
						.where(EmployeeSpecifications.withEmployeeIDNumber(searchForm.getEmployeeIdNumber()))
						.and(EmployeeSpecifications.withFirstName(searchForm.getEmployeeName()))
						.and(EmployeeSpecifications.withEmployeeCreatedBy(searchForm.getCreatedBy()))
						.and(EmployeeSpecifications.withEmployeeCreatedDate(searchForm.getCreatedDate()))
						.and(EmployeeSpecifications.withEmployeeIsDelete(false)));
			} else if (searchForm.getCreatedDate() == null && searchForm.getCompany() != null) {
				employeeList = this.employeeRepo.findAll(Specification
						.where(EmployeeSpecifications.withEmployeeIDNumber(searchForm.getEmployeeIdNumber()))
						.and(EmployeeSpecifications.withFirstName(searchForm.getEmployeeName()))
						.and(EmployeeSpecifications.withEmployeeCreatedBy(searchForm.getCreatedBy()))
						.and(EmployeeSpecifications.withEmployeeCompany(searchForm.getCompany()))
						.and(EmployeeSpecifications.withEmployeeIsDelete(false)));
			} else {
				employeeList = this.employeeRepo.findAll(Specification
						.where(EmployeeSpecifications.withEmployeeIDNumber(searchForm.getEmployeeIdNumber()))
						.and(EmployeeSpecifications.withFirstName(searchForm.getEmployeeName()))
						.and(EmployeeSpecifications.withEmployeeCreatedBy(searchForm.getCreatedBy()))
						.and(EmployeeSpecifications.withEmployeeIsDelete(false)));
			}
		} else {
			employeeList = this.employeeRepo.getAllEmployee();
		}
		List<EmployeeModel> list;

		if (employeeList.size() < startItem) {
			list = Collections.emptyList();
		} else {
			Integer toIndex = Math.min(startItem + pageSize, employeeList.size());
			list = employeeList.subList(startItem, toIndex);
		}

		Page<EmployeeModel> employeePage = new PageImpl<EmployeeModel>(list, PageRequest.of(currentPage, pageSize),
										   employeeList.size());
		return employeePage;
	}

	@Override
	public String save(EmployeeModel employee) {
		String message = new String();
		EmployeeModel employeeForDatabase = new EmployeeModel();
		if (employee.getId() != null) {
			employeeForDatabase = this.getEmployeeFindById(employee.getId());
			employeeForDatabase.setFirstName(employee.getFirstName());
			employeeForDatabase.setEmail(employee.getEmail());
			employeeForDatabase.setLastName(employee.getLastName());
			employeeForDatabase.setmCompanyId(employee.getmCompanyId());
			employeeForDatabase.setCode(employee.getCode());
			employeeForDatabase.setUpdatedBy("Admin");
			employeeForDatabase.setUpdatedDate(LocalDate.now());
			this.employeeRepo.save(employeeForDatabase);
			message = "Data Updated! Data Employee has been updated";
		} else {
			employeeForDatabase.setFirstName(employee.getFirstName());
			employeeForDatabase.setEmail(employee.getEmail());
			employeeForDatabase.setLastName(employee.getLastName());
			employeeForDatabase.setmCompanyId(employee.getmCompanyId());
			employeeForDatabase.setCreatedBy("Admin");
			employeeForDatabase.setCreatedDate(LocalDate.now());
			employeeForDatabase.setIsDelete(false);
			employeeForDatabase.setCode(employee.getCode());
			this.employeeRepo.save(employeeForDatabase);
			message = "Data Saved! New Employee has been add with Employee ID Number " + employeeForDatabase.getCode()
					+ " !";
		}
		return message;
	}

	@Override
	public EmployeeModel getEmployeeFindById(Integer id) {
		return this.employeeRepo.findById(id).orElse(null);
	}

	@Override
	public String delete(EmployeeModel employee) {
		EmployeeModel employeeForDelete = new EmployeeModel();
		employeeForDelete = this.employeeRepo.findById(employee.getId()).orElse(null);
		employeeForDelete.setUpdatedBy("Admin");
		employeeForDelete.setUpdatedDate(LocalDate.now());
		employeeForDelete.setIsDelete(true);
		this.employeeRepo.save(employeeForDelete);
		return "Data Deleted! Data Employee with Employee ID Number " + employeeForDelete.getCode()
				+ "has been deleted !";
	}

	@Override
	public List<CompanyDTO> getDDLCompany() {
		List<CompanyModel> companyList = this.companyRepo.getAllCompany();

		List<CompanyDTO> companyDDLList = new ArrayList<CompanyDTO>();
		for (CompanyModel company : companyList) {
			CompanyDTO companyDto = new CompanyDTO();
			companyDto.setId(company.getId());
			companyDto.setName(company.getName());
			companyDDLList.add(companyDto);
		}
		return companyDDLList;
	}
		
}