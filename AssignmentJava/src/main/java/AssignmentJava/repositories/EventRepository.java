package AssignmentJava.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import AssignmentJava.model.EventModel;

public interface EventRepository extends JpaRepository<EventModel, Integer>, JpaSpecificationExecutor<EventModel> {

	@Query(value= "select * from t_event c where c.is_delete = false order by c.code", nativeQuery = true)
	List<EventModel> getAllEvent();
	
	@Query(value= "select max(id) from t_event", nativeQuery = true)
	Integer getValueId();
}
