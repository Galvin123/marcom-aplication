package AssignmentJava.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import AssignmentJava.Dto.EmployeeDTO;
import AssignmentJava.Dto.EventSearchFormDTO;
import AssignmentJava.model.EventModel;

public interface EventService {

	List<Integer> getPageNumbers(Integer totalPages);
	Page<EventModel> getEventWithPage(Pageable pageable, EventSearchFormDTO searchForm);
	String save(EventModel event);
	EventModel getEventFindById(Integer id);
	String delete(EventModel event);
	List<EmployeeDTO> getDDLEmployee();
	String rejected(EventModel event);
	
}
