package AssignmentJava;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import AssignmentJava.Dto.CompanySearchFormDTO;
import AssignmentJava.model.CompanyModel;
import AssignmentJava.service.CompanyService;

@Controller
@RequestMapping("/company")
public class CompanyController {

	@Autowired
	private CompanyService companyService;
	
	@GetMapping("index")
	public ModelAndView index(@RequestParam("page") Optional<Integer> page, @ModelAttribute CompanySearchFormDTO searchForm, HttpSession session) {
		
		ModelAndView view = new ModelAndView("/company/index");
		List<String> companyNameList = this.companyService.getAllCompanyName();
		List<String> companyCodeList = this.companyService.getAllCompanyCode();
		
        Integer currentPage = page.orElse(1);
		Page<CompanyModel> companyPage = this.companyService.getCompanyWithPage(PageRequest.of(currentPage - 1, 5), searchForm);
		
		Integer totalPages = companyPage.getTotalPages();
		if(totalPages>0) {
			List<Integer> pageNumbers = this.companyService.getPageNumbers(totalPages);
			view.addObject("pageNumbers", pageNumbers);
		}
		String message= null;
		if (session.getAttribute("message") != null) {
	    	message = (String) session.getAttribute("message");
	    }
		CompanySearchFormDTO searchFormDTO = searchForm;
		view.addObject("searchForm", searchFormDTO);
		view.addObject("companyNameList", companyNameList);
		view.addObject("companyCodeList", companyCodeList);
		view.addObject("companyPage", companyPage);
		view.addObject("message", message);
		session.removeAttribute("message");
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/company/add");
		CompanyModel company = new CompanyModel();
		view.addObject("company", company);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute CompanyModel company, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.companyService.save(company);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/company/index");
		}
		else {
			return new ModelAndView("redirect:/company/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/company/edit");
		CompanyModel company = this.companyService.getCompanyFindById(id);
		String title = "Edit Company - " + company.getName() + "(" + company.getCode() + ")";
		view.addObject("company",company);
		view.addObject("title",title);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/company/delete");
		CompanyModel company = new CompanyModel();
		company.setId(id);
		view.addObject("company",company);
		return view;
	}
	
	@PostMapping("deletedata")
	public ModelAndView deletedata(@ModelAttribute CompanyModel company, BindingResult result, HttpSession session) {
		if(!result.hasErrors()) {
			String message = this.companyService.delete(company);
			session.setAttribute("message", message);
			return new ModelAndView("redirect:/company/index");
		}
		else {
			return new ModelAndView("redirect:/company/delete");
		}
	}
	
	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id")Integer id) {
		ModelAndView view = new ModelAndView("/company/details");
		CompanyModel company = this.companyService.getCompanyFindById(id);
		String title = "View Company - " + company.getName() + "(" + company.getCode() + ")";
		view.addObject("company",company);
		view.addObject("title",title);
		return view;
	}
	
}
