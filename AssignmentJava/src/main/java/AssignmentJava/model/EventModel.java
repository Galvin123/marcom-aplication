package AssignmentJava.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Entity
@Table(name="t_event")
public class EventModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11)
	private Integer id;
	
	@Column(name="code", length = 50, nullable = false)
	private String code;
	
	@Column(name="event_name", length = 255, nullable = false)
	private String eventName;
	
	@Column(name="start_date", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate startDate;
	
	@Column(name="end_date", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;
	
	@Column(name="place", length = 255, nullable = true)
	private String place;
	
	@Column(name="budget", length = 50, nullable = true)
	private Long budget;
	
	@ManyToOne
	@JoinColumn(name = "request_by", referencedColumnName = "id", insertable = false, updatable = false, nullable = true)
	private EmployeeModel request;
	
	@Column(name="request_by", length = 11, nullable = false)
	private Integer requestBy;
	
	@Column(name="request_date", nullable = false)
	private LocalDate requestDate;
	
	@ManyToOne
	@JoinColumn(name = "approved_by", referencedColumnName = "id", insertable = false, updatable = false, nullable = true)
	private EmployeeModel approved;
	
	@Column(name="approved_by", length = 11, nullable = true)
	private Integer approvedBy;
	
	@Column(name="approved_date", nullable = true)
	private LocalDate approvedDate;
	
	@ManyToOne
	@JoinColumn(name = "assign_to", referencedColumnName = "id", insertable = false, updatable = false, nullable = true)
	private EmployeeModel assign;
	
	@Column(name="assign_to", length = 11, nullable = true)
	private Integer assignTo;
	
	@Column(name="closed_date", nullable = true)
	private LocalDate closedDate;
	
	@Column(name="note", length = 255, nullable = true)
	private String note;
	
	@ManyToOne
	@JoinColumn(name="status", referencedColumnName = "id", insertable = false, updatable = false, nullable = true)
	public EventView event;
	
	@Column(name="status", length = 1, nullable = true)
	private Integer status;
	
	@Column(name="reject_reason", length = 255, nullable = true)
	private String rejectReason;
	
	@Column(name="is_delete", nullable = false)
	private Boolean isDelete = false;
	
	@Column(name="created_by", length = 50, nullable = false)
	private String createdBy;
	
	@Column(name="created_date", nullable = false)
	private LocalDate createdDate;
	
	@Column(name="updated_by", length = 50, nullable = true)
	private String updatedBy;
	
	@Column(name="updated_date", nullable = true)
	private LocalDate updatedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Long getBudget() {
		return budget;
	}

	public void setBudget(Long budget) {
		this.budget = budget;
	}

	public EmployeeModel getRequest() {
		return request;
	}

	public void setRequest(EmployeeModel request) {
		this.request = request;
	}

	public Integer getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(Integer requestBy) {
		this.requestBy = requestBy;
	}

	public LocalDate getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(LocalDate requestDate) {
		this.requestDate = requestDate;
	}

	public EmployeeModel getApproved() {
		return approved;
	}

	public void setApproved(EmployeeModel approved) {
		this.approved = approved;
	}

	public Integer getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Integer approvedBy) {
		this.approvedBy = approvedBy;
	}

	public LocalDate getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(LocalDate approvedDate) {
		this.approvedDate = approvedDate;
	}

	public EmployeeModel getAssign() {
		return assign;
	}

	public void setAssign(EmployeeModel assign) {
		this.assign = assign;
	}

	public Integer getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(Integer assignTo) {
		this.assignTo = assignTo;
	}

	public LocalDate getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(LocalDate closedDate) {
		this.closedDate = closedDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public EventView getEvent() {
		return event;
	}

	public void setEvent(EventView event) {
		this.event = event;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDate getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDate updatedDate) {
		this.updatedDate = updatedDate;
	}

}
